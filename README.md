Pokemon Fan-made game based in a the Topelt region, where double battles reign supreme.

I plan on working on this as a side project for the forseeable future.
Follow development on my Twitch channel as I learn how to make a pokemon game 
and take feedback in real time.

Twitch: Twitch.tv/thecasuaigamer
Twitter: Twitter.com/thecasuaigamer

Planned Features:

- Extensive use of double battles
- Physical/Special Split
- Trade Stone for needed pokemon
- Slightly lower shiny rate with redone shinies (TBD)
- Rival pokemon and other doubles focused pokemon to feature heavily
- New evolutions for rival/counterpart and paired pokemon (SUPER TBD given support)
- Rebalanced stat changes to make lesser-used pokemon more viable
- Mega-Evolution
- Z-Moves (TBD)
- Up-to-Date Generations
- Custom doubles focused moves (TBD)
- Opposing type gym leaders using most available types (Order subject to change)
    1) Fire/Grass
    2) Ground/Ice
    3) Ghost/Normal
    4) Fighting/Psychic
    5) Steel/Poison
    6) Bug/Flying
    7) Dark/Psychic
    8) Fairy/Dragon
- Elite 4 not specializing in type but perhaps style or strategy
- Rework of AI to include tactical switches and move usage
- Expansion of AI use of items
- AI pokemon more commonly hold items but they are mentioned by the trainer
- Post-Game (TBD)
- Team Unleash whose goals surround the exploitation of Megas
